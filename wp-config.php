<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'econergy' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&ZK!ULxC%X=P/kSdG~62N/ xD.#b(<S=|>;^VJ>Lo0o?js8tHP1s2! Qr=KCT2`@' );
define( 'SECURE_AUTH_KEY',  'Rr`5)5qcd$s>.A*mhHE|jVnY{^PMha.G4[$i,7U%SW<{8Lez!w;Zn(?ZS&psaC^w' );
define( 'LOGGED_IN_KEY',    '#~1DqiX}SA/w(?roapw9CoaBC!_^/ek*m|[Isc,^2&l,oSg(MNM~3(H4?BT6]F;(' );
define( 'NONCE_KEY',        'u7PrEA`0.T$,Acp&P0u{QCrs7YPuwb#`Yx7~NUxs(w{71#@(lsJp.vJ>0v+~`Wu`' );
define( 'AUTH_SALT',        '^+@mn}gcRdp:m<zp>fBZa8agz5Pn$Z!7v&oX)BGt-CPO=wDd0wvnYHKP^c~+hNch' );
define( 'SECURE_AUTH_SALT', 'Ws3npZc5mJt|v7bk*:Xu,@$rr}jiZ*0+-Z)XJ8A48,oyFFGp%UG&~H3&/TC26wB]' );
define( 'LOGGED_IN_SALT',   '19ej.P<H}G-93AUjTMrq=/<4zYpK*Y3}wB_?BzJaC^44-s2Vm(s>mV(0^}qExI(n' );
define( 'NONCE_SALT',       '<oT=XuAnvoy+]of5fXMCS2mPv7k.kb,#^Da KTZL37u%_40XE<;ckv!:[B/,G!yY' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'ee_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
